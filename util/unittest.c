// vim: sw=8:ts=8:noet:ai

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <err.h>

#include "../revstr.h"

extern struct function_desc functions[];
extern ssize_t functions_array_length;
extern void revstr_init(void);
extern void revstr_fini(void);

#define EXIT_ERROR(exitcode)				\
	do {						\
	  err((exitcode), "%s:%d", __FILE__, __LINE__);	\
	} while (0)


// This is the reference implementation that can be blindly trusted :)
static inline void reference_revstr(char *string) {
	char *start = string;
	char *end = string + strlen(string) - 1;
	char tmp;

	while (start < end) {
		tmp = *start;
		*start++ = *end;
		*end-- = tmp;
	}
}


/*
 * Test the given function on a string of the given length
 *
 * The string to reverse is dynamically allocated as part of a larger buffer.
 * The string to reverse is hence in the middle of said buffer,
 * with some variable length padding before and after it.
 *
 * I limit the string length to 1024 characters to avoid stack overflow errors with recursive
 * implementations of revstr and because some impleentations are painfully slow (e.g. pcre2).
 *
 * Once initialized with random characters, the string to reverse is zero terminated
 * and the whole buffer is then duplicated and run against the reference implementation.
 *
 * The function under test is run on the first buffer and the two buffers are then compared.
 *
 * The padding is taken into account when checking for exactness as the function should not
 * affect anything else outside the string to reverse (that's I use data before and after).
 */
static int test_one_pattern(revstr_func func, ssize_t length) {
	// size of padding before and after the string to be reversed
	ssize_t before_len = rand() % 64;
	ssize_t after_len = rand() % 64;

	// How much to allocate in total
	ssize_t alloc_size = before_len + length + 1 + after_len;

	// The working buffer
	char *buf = malloc(alloc_size);
	if (buf == NULL)
		EXIT_ERROR(2);

	// This buffer is used as reference to compare against.
	char *buf_ref = malloc(alloc_size);
	if (buf_ref == NULL) {
		free(buf);
		EXIT_ERROR(2);
	}

	// initialize one of the buffers with (pseudo) random printable characters
	for (int i = 0; i < alloc_size; i++)
		buf[i] = rand() % (127 - 32) + 32;
	buf[before_len + length] = '\0';

	// Copy/initialize the reference buffer
	memcpy(buf_ref, buf, alloc_size);
	reference_revstr(buf_ref + before_len);

	// Call the function to reverse the string on the working buffer
	func(buf + before_len);

	// Compare with the reference buffer
	int result = memcmp(buf, buf_ref, alloc_size) ? 1 : 0;

	free(buf_ref);
	free(buf);

	return result;
}


// Test the given function
static void test_one_function(struct function_desc *fn, int *passed, int *failed) {
	assert(passed != NULL && failed != NULL);

	printf("Testing %s()... ", fn->name); fflush(stdout);
	for (int length = 0; length < 1024; length++) {
		if (test_one_pattern(fn->func, length) == 0)
			(*passed)++;
		else
			(*failed)++;
	}
}


int main(int argc, char **argv) {
	int fn_total = 0, fn_passed = 0;
	time_t seed = time(NULL);

	// Allow reproducible runs by showing/choosing the random seed
	if (argc > 1)
		seed = strtoul(argv[1], NULL, 10);
	printf("Initializing RNG seed to %lu\n", seed);
	srand(seed);

	revstr_init();

	for (int i = 0; i < functions_array_length; i++) {
		struct function_desc *fn = &functions[i];
		int pattern_passed = 0;
		int pattern_failed = 0;
		int pattern_total;

		test_one_function(fn, &pattern_passed, &pattern_failed);
		pattern_total = pattern_passed + pattern_failed;

		if (pattern_failed) {
			printf("%d/%d tests FAILED\n", pattern_failed, pattern_total);
		} else {
			printf("all tests PASSED (%d tests)\n", pattern_total);
			fn_passed++;
		}
		fn_total++;
	}

	if (fn_passed != fn_total) {
		printf("%d/%d functions passed\n", fn_passed, fn_total);
		return 1;
	}

	printf("All tests passed!\n");
	revstr_fini();

	return 0;
}
