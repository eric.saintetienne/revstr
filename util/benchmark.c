// vim: sw=8:ts=8:noet:ai

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <err.h>
#include <locale.h>

#include "../revstr.h"
#include "libfort/fort.h"

extern struct function_desc functions[];
extern ssize_t functions_array_length;
extern void revstr_init(void);
extern void revstr_fini(void);

#define EXIT_ERROR(exitcode)				\
	do {						\
	  err((exitcode), "%s:%d", __FILE__, __LINE__);	\
	} while (0)

#define ITERATION_DIV 10

// Calls the function as many times as possible during 0.2 seconds.
static long bench_one_pattern(revstr_func func, char *string) {
	int iterations = 0;
	clock_t start = clock();

	while ((clock() - start) < CLOCKS_PER_SEC / ITERATION_DIV) {
		func(string);
		iterations++;
	}
	return iterations * ITERATION_DIV;
}

// Fill the given string with random printable characters
static inline void fill_string(char *string, int length) {
	for (int i = 0; i < length; i++)
		string[i] = rand() % (127 - 32) + 32;
	string[length] = '\0';
}

#define MEDIUM_SIZE 256
#define LARGE_SIZE 7280

// Benchmark the given function
static void bench_one_function(struct function_desc *fn, long *res_empty, long *res_short, long *res_long) {
	char buf[LARGE_SIZE + 1];

	assert(res_empty != NULL && res_empty != NULL && res_long != NULL);

	buf[0] = '\0';
	*res_empty = bench_one_pattern(fn->func, buf);

	fill_string(buf, MEDIUM_SIZE);
	*res_short = bench_one_pattern(fn->func, buf);

	fill_string(buf, LARGE_SIZE);
	*res_long = bench_one_pattern(fn->func, buf);
}


int main(int argc, char **argv) {
	ft_table_t *table = ft_create_table();

	revstr_init();

	// Display numbers with commas.
	// (if the locale is missing, that's okay, you won't see the commas)
	setlocale(LC_NUMERIC, "en_US.UTF-8");

	// Setup table style & output header row
	ft_set_border_style(table, FT_SOLID_STYLE);
	ft_set_cell_prop(table, 0, FT_ANY_COLUMN, FT_CPROP_ROW_TYPE, FT_ROW_HEADER);
	ft_set_cell_prop(table, 0, FT_ANY_COLUMN, FT_CPROP_CELL_TEXT_STYLE, FT_TSTYLE_BOLD);
	ft_set_cell_prop(table, 0, FT_ANY_COLUMN, FT_CPROP_TEXT_ALIGN, FT_ALIGNED_CENTER);
	ft_write_ln(table, "Implementation", "empty string", "short string\n(256 chars)", "long string\n(~7k chars)");

	for (int i = 0; i < functions_array_length; i++) {
		struct function_desc *fn = &functions[i];
		long res_empty, res_short, res_long;

		printf("\r%ld%%", (100 * i) / functions_array_length); fflush(stdout);
		bench_one_function(fn, &res_empty, &res_short, &res_long);

		ft_set_cell_prop(table, i + 1, 1, FT_CPROP_TEXT_ALIGN, FT_ALIGNED_RIGHT);
		ft_set_cell_prop(table, i + 1, 2, FT_CPROP_TEXT_ALIGN, FT_ALIGNED_RIGHT);
		ft_set_cell_prop(table, i + 1, 3, FT_CPROP_TEXT_ALIGN, FT_ALIGNED_RIGHT);
		ft_printf_ln(table, "%s()|%'ld|%'ld|%'ld", fn->name, res_empty, res_short,res_long);
	}

	printf("\r%s", ft_to_string(table));

	ft_destroy_table(table);
	revstr_fini();
	return 0;
}
