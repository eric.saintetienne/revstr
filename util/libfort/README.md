# libfort

[libfort](https://github.com/seleznevae/libfort) is a portable, easy-to-use, no-nonsense C library to output simple tables in your terminal.

Copyright &copy; 2017-2020 Seleznev Anton

License: [MIT](https://opensource.org/licenses/MIT)
