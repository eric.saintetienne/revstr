// vim: sw=8:ts=8:noet:ai

#include <string.h>


// Swap two characters
static inline void revswap8(char *p1, char *p2) {
        char tmp = *p1;
        *p1 = *p2;
        *p2 = tmp;
}


void duff_revstr(char *string) {
	size_t length = strlen(string);
	char *start = string;
	char *end = string + length - 1;

	length /= 2;

	if (length == 0)
		return;

	// How many full loops to do
	int n = (length + 7) / 8;

	switch (length % 8) {
		case 0: do {
			revswap8(start++, end--);
		case 7: revswap8(start++, end--);
		case 6: revswap8(start++, end--);
		case 5: revswap8(start++, end--);
		case 4: revswap8(start++, end--);
		case 3: revswap8(start++, end--);
		case 2: revswap8(start++, end--);
		case 1: revswap8(start++, end--);
		} while (--n > 0);
	}
}
