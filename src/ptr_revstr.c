// vim: sw=8:ts=8:noet:ai

#include <string.h>


void ptr_revstr(char *string) {
	char *start = string;
	char *end = string + strlen(string) - 1;
	char tmp;

	while (start < end) {
		tmp = *start;
		*start++ = *end;
		*end-- = tmp;
	}
}
