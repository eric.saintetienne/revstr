// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

struct stack {
	char *base;
	int watermark;
	int size;
};

static inline void stack_push(struct stack *stack, char value) {
	if (stack->watermark < stack->size)
		stack->base[stack->watermark++] = value;
}

static inline void stack_pop(struct stack *stack, char *value) {
	if (stack->watermark > 0)
		*value = stack->base[--stack->watermark];
}

void stack_revstr(char *string) {
	int length = strlen(string);
	struct stack stack = {
		.base = calloc(length, sizeof(char)),
		.size = length,
		.watermark = 0
	};

	if (stack.base == NULL) {
		perror("stack_revstr");
		return;
	}

	for (int i = 0; i < length; i++)
		stack_push(&stack, string[i]);

	for (int i = 0; i < length; i++)
		stack_pop(&stack, &string[i]);

	free(stack.base);
}
