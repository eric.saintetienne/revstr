// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdint.h>


// Swap two characters
static inline void revswap8(char *p1, char *p2) {
        char tmp = *p1;
        *p1 = *p2;
        *p2 = tmp;
}


// Little <-> Big endian conversion
static inline uint64_t EndianSwap(uint64_t val) {
        return (
                (val >> 56) |
                ((val & 0x00ff000000000000ULL) >> 40) |
                ((val & 0x0000ff0000000000ULL) >> 24) |
                ((val & 0x000000ff00000000ULL) >> 8 ) |
                ((val & 0x00000000ff000000ULL) << 8 ) |
                ((val & 0x0000000000ff0000ULL) << 24) |
                ((val & 0x000000000000ff00ULL) << 40) |
                (val << 56)
        );
}


// swap and reverse two 64-bit numbers
static inline void revswap64(uint64_t *p1, uint64_t *p2) {
        uint64_t tmp = *p1;
        *p1 = EndianSwap(*p2);
        *p2 = EndianSwap(tmp);
}


void bits64_revstr(char *string) {
	size_t length = strlen(string);
	char *start = string;
	char *end = string + length;

	length /= 2;

	// How many full 64-bit to copy
	for (int n = length / sizeof(uint64_t); n != 0; n--) {
		revswap64((uint64_t*) start, ((uint64_t*) end) - 1);
		start += sizeof(uint64_t);
		end -= sizeof(uint64_t);
		length -= sizeof(uint64_t);
	}

	// Copy the remaining bytes (less than 8)
	while (length-- != 0)
		revswap8(start++, --end);
}
