# btree.c

[btree.c](https://github.com/tidwall/btree.c) is a portable, easy-to-use, no-nonsense C library to implement B-trees.

Copyright &copy; 2020 [Josh Baker](https://github.com/tidwall)

License: [MIT](https://opensource.org/licenses/MIT)
