// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdlib.h>
#include <stdio.h>


// Allocate a new string, then copy the characters
// in reverse order from the input string and
// finally return that newly allocated string.
static inline char *_malloc_revstr(char *string) {
	int length = strlen(string);
	char *result = malloc(length + 1);

	if (result == NULL) {
		perror("malloc_revstr");
		return NULL;
	}

	for (int i = 0; i < length; i++) {
		result[i] = string[length - i - 1];
	}
	result[length] = '\0';

	return result;
}


void malloc_revstr(char *string) {
	char *allocated = _malloc_revstr(string);

	if (allocated != NULL) {
		strcpy(string, allocated);
		free(allocated);
	}
}
