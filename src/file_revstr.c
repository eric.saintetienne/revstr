// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void file_revstr(char *string) {
	char template[] = "/tmp/revstr-XXXXXX";
	int length = strlen(string);
	int fd = mkstemp(template);

	if (fd == -1) {
		perror("mkstemp");
		return;
	}

	if (write(fd, string, length) != length) {
		perror("write");
		goto cleanup;
	}
	for (int i = 0; i < length; i++) {
		// Go back to the previous character
		lseek(fd, -1, SEEK_CUR);

		// Read one character
		if (read(fd, &string[i], 1) != 1) {
			perror("read");
			goto cleanup;
		};
		// Go back one more as read() has advanced the file pointer by one
		lseek(fd, -1, SEEK_CUR);
	}
cleanup:
	close(fd);
	unlink(template);
}
