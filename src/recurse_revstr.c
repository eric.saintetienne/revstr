// vim: sw=8:ts=8:noet:ai

#include <string.h>


static void _recurse_revstr(char *string, int length) {

	if (length <= 1)
		return;

	char tmp = string[0];
	string[0] = string[length - 1];
	string[length - 1] = tmp;

	_recurse_revstr(string + 1, length - 2);
}


void recurse_revstr(char *string) {
	_recurse_revstr(string, strlen(string));
}
