// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

static inline pcre2_code *pattern_compile(char *pattern) {
	size_t erroroffset;
	int errornumber;
	pcre2_code *re = pcre2_compile(
		(PCRE2_SPTR8) pattern, PCRE2_ZERO_TERMINATED, PCRE2_DOTALL,
		&errornumber, &erroroffset, NULL
	);

	if (re == NULL) {
		PCRE2_UCHAR buffer[256];
		pcre2_get_error_message(errornumber, buffer, sizeof(buffer));
		printf("PCRE2 compilation failed at offset %ld: %s\n", erroroffset, buffer);
		return NULL;
	}
	return re;
}

// Allocate and assemble the regex pattern
static inline char *create_pattern(int length) {
	char *pattern = malloc(3 * length + 2 + 1);

	if (pattern == NULL) {
		perror("create_pattern");
		return NULL;
	}

	strcpy(pattern, "^");
	for (int i = 0; i < length; i++)
		strcat(pattern, "(.)");
	strcat(pattern, "$");

	return pattern;
}

static inline int my_simple_log10(int number) {
	if (number < 10)    return 1;
	if (number < 100)   return 2;
	if (number < 1000)  return 3;
	if (number < 10000) return 4;
	return 5; // Maximum capturing groups: 65536
}

// Allocate and initialie the replacement pattern
// libpcre2 supports only 65536 groups!
static inline char *create_replacement(int length) {
	int width = my_simple_log10(length) + 1;
	char *replacement = malloc(width * length + 1);

	if (replacement == NULL) {
		perror("create_replacement");
		return NULL;
	}

	*replacement = '\0';
	for (int i = 0; i < length; i++) {
		char *end = replacement + strlen(replacement);
		sprintf(end, "$%d", length - i);
	}
	return replacement;
}

static inline char *substitute(pcre2_code *re, char *string, char *replacement, int length) {
	// Allocate space for the groups
	pcre2_match_data *match_data = pcre2_match_data_create(2*length, NULL);

	uint32_t options = PCRE2_ANCHORED | PCRE2_ENDANCHORED;

	PCRE2_SIZE output_length = length + 1;
	PCRE2_UCHAR *output = malloc(output_length);

	// Does the substitution
	pcre2_substitute(
		re,
		(PCRE2_SPTR8) string, PCRE2_ZERO_TERMINATED,
		0, options, match_data, NULL,
		(PCRE2_SPTR8) replacement, PCRE2_ZERO_TERMINATED,
		output, &output_length
	);
	free(match_data);
	return (char*) output;
}

void pcre2_revstr(char *string) {
	int length = strlen(string);
	char *pattern = create_pattern(length);
	if (pattern == NULL)
		return;

	char *replacement = create_replacement(length);
	if (replacement == NULL) {
		free(pattern);
		return;
	}

	// Compile regex into binary
	pcre2_code *re = pattern_compile(pattern);
	if (re == NULL) {
		free(pattern);
		free(replacement);
		return;
	}

	char *output = substitute(re, string, replacement, length);

	// Copy the reversed string into the original one
	strcpy(string, (char*) output);

	free(output);
	free(replacement);
	free(pattern);
}
