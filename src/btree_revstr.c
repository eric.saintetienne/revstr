// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "btree/btree.h"

struct node {
	int ridx; // the index in the string
	char c;
};

// Callback used to compare two nodes. We compare using indices in the string.
static int node_compare(const void *n1, const void *n2, void *udata) {
    return ((struct node*) n1)->ridx < ((struct node*) n2)->ridx;
}

// Assign characters to the string (in user_data) during a tree traversal
static bool node_iter_update_string(const void *_node, void *user_data) {
	const struct node *node = _node;
	char *string = user_data;

	string[node->ridx] = node->c;
	return true;
}

// Helper function to create and populate the tree
static inline struct btree *create_btree_from_string(char *string) {
	struct btree *tree = btree_new(sizeof(struct node), 0, node_compare, NULL);
	int length = strlen(string);

	if (tree == NULL) {
		perror("btree_new");
		return NULL;
	}

	// Load the btree
	for (int i = 0; i < length; i++) {
		struct node *n = malloc(sizeof(struct node));

		if (n == NULL) {
			perror("create_btree_from_string");
			goto error;
		}
		n->ridx = length - i - 1;
		n->c = string[i];
		btree_set(tree, n);
	}
	return tree;

error:
	btree_free(tree);
	return NULL;
}

void btree_pop_max_revstr(char *string) {
	int length = strlen(string);
	struct btree *tree = create_btree_from_string(string);

	if (tree == NULL)
		return;

	for (int i = 0; i < length; i++) {
		struct node *n = btree_pop_max(tree);
		string[i] = n->c;
	}

	// Free the btree and its nodes
	btree_free(tree);
}

void btree_descent_revstr(char *string) {
	struct btree *tree = create_btree_from_string(string);

	// Traverse the tree and replace the characters in "string" by the one at
	// the reverse index. Note that the traversal order doesn't matter.
	btree_ascend(tree, NULL, node_iter_update_string, string);

	// Free the btree and its nodes
	btree_free(tree);
}
