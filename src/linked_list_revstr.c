// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdlib.h>


struct node {
	char c;
	struct node *prev;
};


void linked_list_revstr(char *string) {
	int length = strlen(string);
	struct node *tail = NULL;

	for (int i = 0; i < length; i++) {
		struct node *n = (struct node*) malloc(sizeof(struct node));

		if (n == NULL)
			goto free_list;
		n->c = string[i];
		n->prev = tail;
		tail = n;
	}
	struct node *t = tail;
	for (int i = 0; i < length; i++) {
		string[i] = t->c;
		t = t->prev;
	}

free_list:
	while (tail != NULL) {
		struct node *tmp_node = tail->prev;
		free(tail);
		tail = tmp_node;
	}
}
