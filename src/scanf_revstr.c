// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>


void scanf_revstr(char *string) {
	int length = strlen(string);

	char *format = calloc(2 * length + 1, sizeof(char));
	if (format == NULL) {
		perror("scanf_revstr");
		return;
	}

	char *tmp = calloc(length, sizeof(char));
	if (tmp == NULL) {
		perror("scanf_revstr");
		goto error_free_format;
	}

	char **args_list = calloc(length, sizeof(char*));
	if (format == NULL) {
		perror("scanf_revstr");
		goto error_free_args_list;
	}

	for (int i = 0; i < length; i++) {
		strcat(format, "%c");
	}

	// Assemble the list of arguments to use in va_list
	for (int i = 0; i < length; i++) {
		args_list[i] = tmp + length - i - 1;
	}

	// Setup the va_list according to System-V ABI
	// see https://github.com/hjl-tools/x86-psABI/wiki/x86-64-psABI-1.0.pdf
	va_list va;
	va->gp_offset = 0x30;
	va->fp_offset = 0;
	va->overflow_arg_area = args_list;
	va->reg_save_area = args_list;

	// Gently ask scanf to reverse the string into tmp
	vsscanf(string, format, va);

	// Copy the temporary buffer into the string
	memcpy(string, tmp, length);

	// Clean-up
	free(tmp);

error_free_args_list:
	free(args_list);
error_free_format:
	free(format);
}
