// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdio.h>

struct circular_buffer {
	char *base;
	unsigned int size;
};

static inline void cbuf_rotate_right(struct circular_buffer *cbuf) {
	char *buf = cbuf->base;
	int length = cbuf->size;
	char last = buf[length];

	for (int i = length; i > 0; i--)
		buf[i] = buf[i-1];
	buf[0] = last;
}

static inline void cbuf_swap(struct circular_buffer *cbuf, int a, int b) {
	char *buf = cbuf->base;
	char tmp = buf[a];

	buf[a] = buf[b];
	buf[b] = tmp;
}

void circular_revstr(char *string) {
	int length = strlen(string);
	struct circular_buffer cbuf = {
		.base = string,
		.size = length - 1
	};

	for (int i = 0; i < length; i++) {
		cbuf_rotate_right(&cbuf);

		// Bubble-up the character we've just rotated
		for (int j = 0; j < i; j++)
			cbuf_swap(&cbuf, j, j + 1);
	}
}
