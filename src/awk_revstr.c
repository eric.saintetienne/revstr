// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>

static const char awk_program[] = \
	"{x = \"\"; for (i = length; i != 0; i--) x=x substr($0,i,1); print x; system(\"\")}";

static int main_to_sub[2]; // Store the two ends of first pipe
static int sub_to_main[2]; // Store the two ends of second pipe

static pid_t pid;

void awk_init(char *string) {

	if (pipe(sub_to_main) == -1) {
		perror("pipe out");
		exit(1);
	}

	if (pipe(main_to_sub) == -1) {
		perror("pipe in");
		close(sub_to_main[0]);
		close(sub_to_main[1]);
		exit(1);
	}

	pid = fork();
	if (pid < 0) {
		perror("fork");
		close(main_to_sub[0]);
		close(main_to_sub[1]);
		close(sub_to_main[0]);
		close(sub_to_main[1]);
		exit(1);
	}
	if (pid == 0) { // Child process
		dup2(main_to_sub[0], STDIN_FILENO);
		dup2(sub_to_main[1], STDOUT_FILENO);

		close(sub_to_main[0]);
		close(sub_to_main[1]);
		close(main_to_sub[0]);
		close(main_to_sub[1]);

		execlp("awk", "awk", awk_program, NULL);
		perror("is awk in the path? (or even installed?)\n");
		exit(1);
	}
	printf("awk spawned as pid %d\n", pid);
}

void awk_fini(void) {
	printf("Waiting for awk to terminate (pid %d)\n", pid);
	close(main_to_sub[1]);

	// Wait for awk to terminate
	int status;
	waitpid(pid, &status, WNOHANG);

	close(sub_to_main[1]);
	close(main_to_sub[0]);
	close(sub_to_main[0]);
}

void awk_revstr(char *string) {
	int length = strlen(string);

	if (length < 2)
		return;

	// Terminate the string for awk
	string[length] = '\n';

	// Write input string
	int count = write(main_to_sub[1], string, length + 1);
	if (count != length + 1) {
		perror("write");
		return;
	}

	// Read output from child
	count = read(sub_to_main[0], string, length + 1);
	if (count <= 0)
		perror("read");

	string[length] = '\0';
}
