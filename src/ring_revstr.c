// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdio.h>

struct ring {
	unsigned int size;
	unsigned int head;
	char *buffer;
};

static inline void ring_rotate_right(struct ring *ring) {
	ring->head += ring->size - 1; // Avoid entering negative territory
	ring->head %= ring->size;
}

static inline char ring_get_item(struct ring *ring, int pos) {
	int offset = (ring->head + pos) % ring->size;
	return ring->buffer[offset];
}

static inline void ring_set_item(struct ring *ring, int pos, char value) {
	int offset = (ring->head + pos) % ring->size;
	ring->buffer[offset] = value;
}

static inline void ring_swap_items(struct ring *ring, int a, int b) {
	char tmp = ring_get_item(ring, a);
	ring_set_item(ring, a, ring_get_item(ring, b));
	ring_set_item(ring, b, tmp);
}

void ring_revstr(char *string) {
	int length = strlen(string);
	struct ring ring = {
		.size = length,
		.buffer = string,
		.head = 0
	};

	for (int i = 0; i < length; i++) {
		ring_rotate_right(&ring);

		// Bubble-up the character we've just rotated
		for (int j = 0; j < i; j++)
			ring_swap_items(&ring, j, j + 1);
	}
}
