// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdio.h>

void bubble_revstr(char *string) {
	int i, length = strlen(string);

	for (int j = 0; j < length / 2; j++) {
		for (i = j; i < length - j - 1; i++) {
			char tmp = string[i];
			string[i] = string[i + 1];
			string[i + 1] = tmp;
		}
		while (--i > j) {
			char tmp = string[i];
			string[i] = string[i - 1];
			string[i - 1] = tmp;
		}
	}
}
