// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdlib.h>
#include <stdio.h>


void random_revstr(char *string) {
	int length = strlen(string);
	char *done = calloc(length, sizeof(char));

	if (done == NULL) {
		perror("random_revstr");
		return;
	}

	for (int i = 0; i < length / 2; ) {
		int idx = rand() % (length / 2);
		char tmp;

		if (done[idx] == 0) {
			tmp = string[idx];
			string[idx] = string[length - idx - 1];
			string[length - idx - 1] = tmp;
			done[idx]++;
			i++;
		}
	}
	free(done);
}
