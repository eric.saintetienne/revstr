// vim: sw=8:ts=8:noet:ai

#include <string.h>


static void _partition_revstr(char *start, int length) {
	char tmp[length/2];
	int midpoint = length - length/2;  // right block offset

	if (length < 2)
		return;

	// Swap the first half and the second half of the given block
	memcpy(tmp, start, length/2);
	memcpy(start, start + midpoint, length/2);
	memcpy(start + midpoint, tmp, length/2);

	// Recurse into each sub block
	_partition_revstr(start, length/2);
	_partition_revstr(start + length - length/2, length/2);
}

void partition_revstr(char *string) {
	_partition_revstr(string, strlen(string));
}
