// vim: sw=8:ts=8:noet:ai

#include <stdlib.h>

#include "../revstr.h"

#define FUNC_ENTRY(_func_sym) {	  \
	.name=#_func_sym, \
	.func=_func_sym	  \
}

struct function_desc functions[] = {
	FUNC_ENTRY(ptr_revstr),
	FUNC_ENTRY(array_revstr),
	FUNC_ENTRY(array_revstr_simpler),
	FUNC_ENTRY(array_revstr_xor),
	FUNC_ENTRY(recurse_revstr),
	FUNC_ENTRY(malloc_revstr),
	FUNC_ENTRY(random_revstr),
	FUNC_ENTRY(duff_revstr),
	FUNC_ENTRY(bits64_revstr),
	FUNC_ENTRY(linked_list_revstr),
	FUNC_ENTRY(stack_revstr),
	FUNC_ENTRY(bubble_revstr),
	FUNC_ENTRY(partition_revstr),
	FUNC_ENTRY(scanf_revstr),
	FUNC_ENTRY(btree_descent_revstr),
	FUNC_ENTRY(btree_pop_max_revstr),
	FUNC_ENTRY(awk_revstr),
	FUNC_ENTRY(circular_revstr),
	FUNC_ENTRY(ring_revstr),
	FUNC_ENTRY(file_revstr),
	FUNC_ENTRY(pcre2_revstr),
};

ssize_t functions_array_length = sizeof(functions) / sizeof(functions[0]);

// Initialization function to run once only, at program start
void revstr_init(void) {
	awk_init();
}

// De-initialization function to run once only, at program end
void revstr_fini(void) {
	awk_fini();
}

