#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define CL_USE_DEPRECATED_OPENCL_1_0_APIS
#define CL_TARGET_OPENCL_VERSION 100
#include <CL/opencl.h>

static cl_platform_id cpPlatform = 0; // OpenCL platform
static cl_device_id device_id = 0;    // device ID
static cl_context context = 0;        // context
static cl_command_queue queue = 0;    // command queue
static cl_program program = 0;        // program
static cl_kernel kernel = 0;          // kernel

// OpenCL kernel. Each work item takes care of one element of in and out
static const char *kernelSource = "			\
__kernel void revstr(__global char *in,			\
                     const unsigned int length)  	\
{							\
	/* Get our global thread ID */			\
	int id = get_global_id(0);			\
							\
	/* Make sure we do not go out of bounds */	\
	if (id < length / 2) {				\
		char tmp = in[id];			\
		in[id] = in[length - id - 1];		\
		in[length - id - 1] = tmp;		\
	}						\
}";

void opencl_revstr(char *string) {
	int length = strlen(string);  // vector length
	size_t bytes = ((length + 7) / 8) * 8;  // Number of bytes in each vector
	cl_mem device_mem_in;  // Device input buffer
	size_t globalSize = length;  // All working nodes
	size_t localSize = 1;  // Number of working nodes in each working group
	cl_int err;

	if (length < 2)
		return;

	// Allocate a device buffer
	device_mem_in = clCreateBuffer(context, CL_MEM_USE_HOST_PTR, bytes, string, &err);
	if (device_mem_in == 0 || err != CL_SUCCESS) {
		printf("Unable to create OpenCL device buffer\n");
		return;
	}

	// Set the parameters of the calculation kernel
	err  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &device_mem_in);
	err |= clSetKernelArg(kernel, 1, sizeof(int), &length);
	if (err != CL_SUCCESS) {
		printf("Unable to set OpenCL program arguments\n");
		goto release_buffer;
	}

	// Execute the kernel over the entire range of the data set)
	err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);
	if (err != CL_SUCCESS) {
		printf("Unable to execute OpenCL program\n");
		goto release_buffer;
	}

	// Wait for the command queue to get serviced
	if (clFinish(queue) != CL_SUCCESS) {
		printf("Execute of OpenCL program failed\n");
		goto release_buffer;
	}

release_buffer:
	clReleaseMemObject(device_mem_in);
}

void opencl_init(void) {
	cl_int err;

	// Get platform ID
	err = clGetPlatformIDs(1, &cpPlatform, NULL);
	if (err != CL_SUCCESS) {
		printf("OpenCL platform not found\n");
		return;
	}

	// Get device ID, related to the platform
	err = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_CPU, 1, &device_id, NULL);
	if (err == CL_DEVICE_NOT_FOUND) {
		printf("No OpenCL capable devices found\n");
		return;
	}

	// According to the device ID, get the context
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	if (err != CL_SUCCESS) {
		printf("Unable to create OpenCL context\n");
		return;
	}

	// According to the context, create a command queue on the device
	queue = clCreateCommandQueue(context, device_id, 0, &err);
	if (queue == 0 || err != CL_SUCCESS) {
		printf("Unable to create OpenCL command queue\n");
		goto release_context;
	}

	// Create calculation program based on OpenCL source program
	program = clCreateProgramWithSource(context, 1, (const char **) & kernelSource, NULL, &err);
	if (program == 0 || err != CL_SUCCESS) {
		printf("Unable to create OpenCL program\n");
		goto release_cmd_queue;
	}

	// Create executable program
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (err != CL_SUCCESS) {
		printf("Unable to build OpenCL program\n");
		goto release_program;
	}

	// Create a kernel program in the program created above
	kernel = clCreateKernel(program, "revstr", &err);
	if (kernel == 0 || err != CL_SUCCESS) {
		printf("Unable to create OpenCL kernel\n");
		goto release_program;
	}
	return;

release_program:
	clReleaseProgram(program);
release_cmd_queue:
	clReleaseCommandQueue(queue);
release_context:
	clReleaseContext(context);
	exit(1);
}

void opencl_release(void) {
	if (kernel != 0)
		clReleaseKernel(kernel);
	if (program != 0)
		clReleaseProgram(program);
	if (queue != 0)
		clReleaseCommandQueue(queue);
	if (context != 0)
		clReleaseContext(context);

}
