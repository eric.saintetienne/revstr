// vim: sw=8:ts=8:noet:ai

#include <string.h>
#include <stdlib.h>


void array_revstr(char *string) {
	ssize_t length = strlen(string);
	char tmp;

	for (int i = 0; i < length / 2; i++) {
		tmp = string[i];
		string[i] = string[length - i - 1];
		string[length - i - 1] = tmp;
	}
}


// Like array_revstr but maintaining indices
void array_revstr_simpler(char *string) {
	int i = 0, j = strlen(string) - 1;
	char tmp;

	while (i < j) {
		tmp = string[i];
		string[i++] = string[j];
		string[j--] = tmp;
	}
}

// Like the simpler array, but using XOR to swap characters
void array_revstr_xor(char *string) {
	for (int i = 0, j = strlen(string) - 1;
	     i < j;
	     i++, j--) {
            string[i] ^= string[j];
            string[j] ^= string[i];
            string[i] ^= string[j];
        }
}
