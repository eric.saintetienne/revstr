[![pipeline status](https://gitlab.com/eric.saintetienne/revstr/badges/main/pipeline.svg)](https://gitlab.com/eric.saintetienne/revstr/-/commits/main)

This repository stems from Jacob Sorber's educational video entitled _"Reversing Strings (in C) and Divergent Thinking"_ available on YouTube at https://www.youtube.com/watch?v=dcBcgPGIMpo in which he compares a handful of different implementations of string reversal.

Contrary to Jacob who also dealt in his video with implementations that reverse string into a separate buffer, I only consider in-place string reversal (mostly).

The code below is for educational purposes, don't use it in any production environment (although some implementations are sound, most are exploratory, just for fun... you've been warned!)

[[_TOC_]]

I first present the implementations that Jacob included in his video, then the ones I came up with after having watched Jacob's video.

## Approaches described in Jacob's video
For further details about how each function works, please refer to Jacob's video.

### [Using arrray indices](src/array_revstr.c)
A trivial use of array to reverse a string in-place:
```c
void array_revstr(char *string) {
	ssize_t length = strlen(string);
	char tmp;

	for (int i = 0; i < length / 2; i++) {
		tmp = string[i];
		string[i] = string[length - i - 1];
		string[length - i - 1] = tmp;
	}
}
```

As explained in Jacob's video, the loop can be made simpler by using an extra index:
```c
void array_revstr_simpler(char *string) {
	int i = 0, j = strlen(string) - 1;
	char tmp;

	while (i < j) {
		tmp = string[i];
		string[i++] = string[j];
		string[j--] = tmp;
	}
}
```
### [Using pointers](src/ptr_revstr.c)
A prototypical use of pointers:
```c
void ptr_revstr(char *string) {
	char *start = string;
	char *end = string + strlen(string) - 1;
	char tmp;

	while (start < end) {
		tmp = *start;
		*start++ = *end;
		*end-- = tmp;
	}
}
```
### [Recursive](src/recurse_revstr.c)
This recursive implementation takes an extra argument that contains the length:
```c
static void recurse_revstr(char *string, int length) {

	if (length <= 1)
		return;

	char tmp = string[0];
	string[0] = string[length - 1];
	string[length - 1] = tmp;

	recurse_revstr(string + 1, length - 2);
}
```
### [Malloc](src/malloc_revstr.c)
The main issue with this implementation is that the caller need to know he's the owner of the allocated string.
```c
static inline char *_malloc_revstr(char *string) {
	int length = strlen(string);
	char *result = malloc(length + 1);

	if (result == NULL) {
		perror("malloc_revstr");
		return NULL;
	}

	for (int i = 0; i < length; i++) {
		result[i] = string[length - i - 1];
	}
	result[length] = '\0';

	return result;
}

```
### [Random access](src/random_revstr.c)
An original but ineficient version.
```c
void random_revstr(char *string) {
	int length = strlen(string);
	char *done = calloc(length, sizeof(char));

	if (done == NULL) {
		perror("random_revstr");
		return;
	}

	for (int i = 0; i < length / 2; ) {
		int idx = rand() % (length / 2);
		char tmp;

		if (done[idx] == 0) {
			tmp = string[idx];
			string[idx] = string[length - idx - 1];
			string[length - idx - 1] = tmp;
			done[idx]++;
			i++;
		}
	}
	free(done);
}
```

## Other algorithmic approaches
These are pure "C" approaches that are rather portable (as much as C is portable!)

### [XOR swap](src/array_revstr.c)
`XOR` can be used to swap characters without a temporary variable, thanks to the idempotence property of the XOR function:
```c
void array_revstr_xor(char *string) {
	for (int i = 0, j = strlen(string) - 1;
	     i < j;
	     i++, j--) {
            string[i] ^= string[j];
            string[j] ^= string[i];
            string[i] ^= string[j];
        }
}
```
This code is essentially the same as `array_revstr_simpler()` (see [Using array indices](#using-arrray-indices)) but with a different method of swapping characters using XOR.

### [Duff device](src/duff_revstr.c)
You can read more about Duff devices in the [dedicated Wikipedia page](https://en.wikipedia.org/wiki/Duff%27s_device).
```c
void duff_revstr(char *string) {
	size_t length = strlen(string);
	char *start = string;
	char *end = string + length - 1;

	length /= 2;

	if (length == 0)
		return;

	// How many full loops to do
	int n = (length + 7) / 8;

	switch (length % 8) {
		case 0: do {
			revswap8(start++, end--);
		case 7: revswap8(start++, end--);
		case 6: revswap8(start++, end--);
		case 5: revswap8(start++, end--);
		case 4: revswap8(start++, end--);
		case 3: revswap8(start++, end--);
		case 2: revswap8(start++, end--);
		case 1: revswap8(start++, end--);
		} while (--n > 0);
	}
}
```
Where `revswap8()` simply swap two characters.

### [Linked list](src/linked_list_revstr.c)
While progressing through the input string, a backwards linked list is created. The string is then reversed by going again through the string and simultaneously traversing backwards the linked list. Finally, the list is freed.

```c
struct node {
        char c;
        struct node *prev;
};

void linked_list_revstr(char *string) {
	int length = strlen(string);
	struct node *tail = NULL;

	for (int i = 0; i < length; i++) {
		struct node *n = (struct node*) malloc(sizeof(struct node));

		if (n == NULL)
			goto free_list;
		n->c = string[i];
		n->prev = tail;
		tail = n;
	}
	struct node *t = tail;
	for (int i = 0; i < length; i++) {
		string[i] = t->c;
		t = t->prev;
	}

free_list:
	while (tail != NULL) {
		struct node *tmp_node = tail->prev;
		free(tail);
		tail = tmp_node;
	}
}
```

### [Software stack](src/stack_revstr.c)
A software stack (or LIFO for Last In First Out) is well suited to reverse a string:

```c
void stack_revstr(char *string) {
	int length = strlen(string);
	struct stack stack = {
		.base = calloc(length, sizeof(char)),
		.size = length,
		.watermark = 0
	};

	if (stack.base == NULL) {
		perror("stack_revstr");
		return;
	}

	for (int i = 0; i < length; i++)
		stack_push(&stack, string[i]);

	for (int i = 0; i < length; i++)
		stack_pop(&stack, &string[i]);

	free(stack.base);
}
```

### [Bubbling](src/bubble_revstr.c)
Just like in [the bubble sort](https://en.wikipedia.org/wiki/Bubble_sort), the idea is to repeatedly go up and down the input string, bringing up the lowest character we haven't swapped and bringing down the corresponding highest character, then repeat while converging towards the center of the string, until all characters are swapped and the string reversed.
```c
void bubble_revstr(char *string) {
	int i, length = strlen(string);

	for (int j = 0; j < length / 2; j++) {
		for (i = j; i < length - j - 1; i++) {
			char tmp = string[i];
			string[i] = string[i + 1];
			string[i + 1] = tmp;
		}
		while (--i > j) {
			char tmp = string[i];
			string[i] = string[i - 1];
			string[i - 1] = tmp;
		}
	}
}
```
### [Partitioning](src/partition_revstr.c)
This implementation splits up the buffer into two buffers of equal size and swaps them using `memcpy()`. Then it repeats (by recursion) on each of the two sub-blocks until all characters have been swapped.

```c
static void partition_revstr(char *start, int length) {
	char tmp[length/2];
	int midpoint = length - length/2;  // right block offset

	if (length < 2)
		return;

	// Swap the first half and the second half of the given block
	memcpy(tmp, start, length/2);
	memcpy(start, start + midpoint, length/2);
	memcpy(start + midpoint, tmp, length/2);

	// Recurse into each sub block
	partition_revstr(start, length/2);
	partition_revstr(start + length - length/2, length/2);
}
```
### [Binary tree](src/btree_revstr.c)
One can use binary tree in several ways. Here are two approaches.

In each case we build the binary tree by traversing the input string. Each node of the tree contains the character as well as the index in the input string (actually I use the index in the reverse string, just for practical reasons):
```c
struct node {
	int ridx; // the index in the reverse string
	char c;   // the character at index ridx
};
```

#### Pop the maximum

This implementation traverse the input string sequentially, and at each iteration pops the node with the maximum `ridx` value and use it to assign the character in the string at the current location:
```c
void btree_pop_max_revstr(char *string) {
	int length = strlen(string);
	struct btree *tree = create_btree_from_string(string);

	if (tree == NULL)
		return;

	for (int i = 0; i < length; i++) {
		struct node *n = btree_pop_max(tree);
		string[i] = n->c;
	}

	// Free the btree and its nodes
	btree_free(tree);
}
```
#### Tree traversal
In this implementation, we traverse the tree and for each node, we update the corresponding character in the input string (at index `ridx`) with the character in the node.
```c
// Assign characters to the string (in user_data) during a tree traversal
static bool node_iter_update_string(const void *_node, void *user_data) {
	const struct node *node = _node;
	char *string = user_data;

	string[node->ridx] = node->c;
	return true;
}

void btree_descent_revstr(char *string) {
	struct btree *tree = create_btree_from_string(string);

	// Traverse the tree and replace the characters in "string" by the one at
	// the reverse index. Note that the traversal order doesn't matter.
	btree_ascend(tree, NULL, node_iter_update_string, string);

	// Free the btree and its nodes
	btree_free(tree);
}
```
Note: the tree serves as storage of indices and characters, so the traversal order doesn't matter as long as all the nodes have been visited just once.

### [Circular buffer](src/circular_revstr.c)
A circular buffer is a common datastructure that can be perverted to reverse a string.

The principle is to rotate the circular buffer completely once, and at each step make the character that was sent to the front of the buffer "bubble up" to its final position:
```c
void circular_revstr(char *string) {
	int length = strlen(string);
	struct circular_buffer cbuf = {
		.base = string,
		.size = length - 1
	};

	for (int i = 0; i < length; i++) {
		rotate_right(&cbuf);

		// Bubble-up the character we've just rotated
		for (int j = 0; j < i; j++)
			cbuf_swap(&cbuf, j, j + 1);
	}
}
```
This algorithm very similar to the [Bubbling](#bubbling) case.

### [Ring buffer](src/ring_revstr.c)

The previous circular buffer was a cheat as it moved the whole buffer around every time, to keep indexes inside the buffer as usual (item zero at buffer index zero). It's easier to reason with, before moving to a ring implementation. In a ring we rotate virtually, by maintaining an `head` index which is the (virtual) index zero. The result is that less memory is moved around:
```c
void ring_revstr(char *string) {
	int length = strlen(string);
	struct ring ring = {
		.size = length,
		.buffer = string,
		.head = 0
	};

	for (int i = 0; i < length; i++) {
		ring_rotate_right(&ring);

		// Bubble-up the character we've just rotated
		for (int j = 0; j < i; j++)
			ring_swap_items(&ring, j, j + 1);
	}
}
```

The implementation is very close to the circular buffer case, because both their high level API are the same. The differences between a circular buffer and a ring lie in how each datastructure is handled internally.

Even though less data is moved around, the overall performance is appauling, I think that's due to executing a bit more code (calling functions that do modulus calculations) in the critical path.

### [File backing](src/file_revstr.c)
This implementation writes the input string into a temporary file and steps backwards into the file, using `lseek()` with `SEEK_CUR` (the current file position), reading one character at a time:
```c
void file_revstr(char *string) {
	char template[] = "/tmp/revstr-XXXXXX";
	int length = strlen(string);
	int fd = mkstemp(template);

	if (fd == -1) {
		perror("mkstemp");
		return;
	}

	if (write(fd, string, length) != length) {
		perror("write");
		goto cleanup;
	}

	for (int i = 0; i < length; i++) {
		// Go back to the previous character
		lseek(fd, -1, SEEK_CUR);

		// Read one character
		if (read(fd, &string[i], 1) != 1) {
			perror("read");
			goto cleanup;
		};
		// Go back one more as read() has advanced the file pointer by one
		lseek(fd, -1, SEEK_CUR);
	}
cleanup:
	close(fd);
	unlink(template);
}
```
This is inefficient as it makes two system calls (`lseek()` and `read()`) per character.

## Non-portable approaches
These implementation only works in a specific environment, for example they need some specific hardware or some library to be installed on the host.

### [64-bits optimized](src/64bits_revstr.c)
When performance matters, this version will perform very well on 64-bit capable architectures:
```c
void bits64_revstr(char *string) {
	size_t length = strlen(string);
	char *start = string;
	char *end = string + length;

	length /= 2;

	// How many full 64-bit to copy
	for (int n = length / sizeof(uint64_t); n != 0; n--) {
		revswap64((uint64_t*) start, ((uint64_t*) end) - 1);
		start += sizeof(uint64_t);
		end -= sizeof(uint64_t);
		length -= sizeof(uint64_t);
	}

	// Copy the remaining bytes (less than 8)
	while (length-- != 0)
		revswap8(start++, --end);
}
```
Where `revswap8()` swaps two characters and `revswap64()` swaps two 64-bit words and at the same time changes the endianness, so they're mirror of each other.

Caveat: architecture(s) that do not support unaligned 64-bit accesses will either crash (`SIGBUS`) or return invalid data.

### [OpenCL](src/opencl_revstr.c)
OpenCL is able to efficiently run simple programs in parallel by leveraging hardware like GPU and dedicated accelerators.

The OpenCL program is very similar to the C array version of revstr:
```c
__kernel void revstr(__global char *in, const unsigned int length) {
	/* Get our global thread ID */
	int id = get_global_id(0);

	/* Make sure we do not go out of bounds */
	if (id < length / 2) {
		char tmp = in[id];
		in[id] = in[length - id - 1];
		in[length - id - 1] = tmp;
	}
}
```
Because OpenCL requires specific hardware to run, I didn't include it in the default branch, it's available in the [opencl branch](-/tree/opencl).

The benchmark at the end of this document shows that OpenCL is surprisingly slow, and I think that's because the OpenCL driver I use is CPU based (no hardware parallelism).

### [Using `vsscanf()`](src/scanf_revstr.c)
`scanf()` family of functions can apply a printf-style format string on any input string and place the result of the parsing into memory locations provided in arguments (pointers). For example:
```c
	char c, w[5];
	int i;
	int count = sscanf("Z 42 word", "%c %i %s", &c, &i, w);
```
The code above will parse the string according to the pattern and place:
* `'Z'` in `c`
* `42` in `i`
* `"word"` in `w`

as well as `3` in `count` as this is the number of fields `sscanf()` could parse.

To reverse a string using `sscanf()`, we need to create:
* a format string that will parse consecutive characters: `"%c%c%c..."` (as many `%c` as there are characters in the input string)
* a number of pointers to characters such that they each point to a character in the input string (at the "reversed" location).

We can then pass the input string, the format string and all the pointers to `sscanf()` and it will go through every character (according to the format string) and fill-in the characters pointed to by the pointers. For instance:
```c
	char *string = strdup("12345");
	char *format = "%c%c%c%c%c";
	char tmp[6];
	sscanf(buf, format, &tmp[4], &tmp[3], &tmp[2], &tmp[1], &tmp[0]);
	memcpy(buf, tmp, 5);
```

Obviously, it's not possible to use pointers into the original string as `sscanf()` would then start modifying the original string, which would not work for the second half of the string as characters have just been overwritten with characters from the first half! So I use a temporary array of characters as "target memory" to the pointers, and then use `memcpy()`  to copy the reversed string from the temporary buffer into the input string.

There's one more problem: the length of the input string is not known in advance, so let's see how it is possible to pass a variable number of pointers to `sscanf()`.

`vsscanf()` is a variant of `sscanf()` that handles a variable number of arguments. I won't enter into too much details as there is a plethora of tutorials on variadic functions, but those tutorials wouldn't help as the usage I want to make of `vsscanf()` is not common: normally we call `va_start()` **from within a variadic function** to initialize the `va_list` variable and later pass it to `vsscanf()`, but `scanf_revstr()` isn't variadic in the first place, so I can't call `va_start()` (the compiler doesn't allow it).

To initialize `va_list` with my own list of pointers, I need to set the individual fields myself, so I had to dig in the [SystemV ABI](https://github.com/hjl-tools/x86-psABI/wiki/x86-64-psABI-1.0.pdf) document, which describes how `va_list` works, and by trial and error I could make it work on intel 64bits (named "AMD64"), in a nutshell:
```c
void scanf_revstr(char *string) {
	int length = strlen(string);

	char *format = calloc(2 * length + 1, sizeof(char));
	char *tmp = calloc(length, sizeof(char));
	char **args_list = calloc(length, sizeof(char*));

	for (int i = 0; i < length; i++) {
		strcat(format, "%c");
	}

	// Assemble the list of arguments to use in va_list
	for (int i = 0; i < length; i++) {
		args_list[i] = tmp + length - i - 1;
	}

	// Setup the va_list according to System-V ABI
	va_list va;
	va->gp_offset = 0x30;
	va->fp_offset = 0;
	va->overflow_arg_area = args_list;
	va->reg_save_area = args_list;

	// Gently ask scanf to reverse the string into tmp
	vsscanf(string, format, va);

	// Copy the temporary buffer into the string
	memcpy(string, tmp, length);
}
```
Warning: fiddling this way with `va_list` is non-standard, it's not something anyone should do as it's a datastructure internal to the C library, it's also not portable as it targets specifically the AMD64 ABI, but the goal of the `revstr` project is to have fun, not to write standard and portable code :metal:

The `0x30` value comes from the fact that the AMD64 ABI passes 6 arguments via registers then the rest on the stack.

The [source code of `scanf_revstr()`](src/scanf_revstr.c) is more thorough with deallocations.

Note: Memory could have been saved by using only half of the temporary buffer.

### [Piping to/from an external program](src/awk_revstr.c)
Provided forking and piping to an external program like `awk` is in place as implemented in [src/awk_revstr.c](src/awk_revstr.c), the following code send a string to the external program and read the response:
```c
void awk_revstr(char *string) {
	int length = strlen(string);

	if (length < 2)
		return;

	// Terminate the string for awk
	string[length] = '\n';

	// Write input string
	int count = write(main_to_sub[1], string, length + 1);
	if (count != length + 1) {
		perror("write");
		return;
	}

	// Read output from child
	count = read(sub_to_main[0], string, length + 1);
	if (count <= 0)
		perror("read");

	string[length] = '\0';
}
```
External programs `like` awk or `sed` expect carriage return terminated lines and in return output carriage return terminated lines.

The `awk` program that reverse lines is trivial (except for `system("")` which flush the output):
```awk
{
    x = ""
    for (i = length; i != 0; i--)
        x=x substr($0, i, 1);
    print x;
    system("")
}
```
Note: GNU `awk` is necessary as other implementations like `mawk` won't work!

For the small story, I first tried with `sed` and [this awful one-liner expression to reverse a string](https://stackoverflow.com/questions/48246212/sed-string-reverse#answer-48250060) but it was so slow with strings of a few hundreds characters that I gave up in favor of `awk`.

`rev` is also an excellent choice, except that it can't process strings "on demand" like a typical Unix filter utility typically does: instead `rev` reads the whole file in memory!

### [PCRE](src/pcre2_revstr.c)
The **Perl Compatible Regular Expressions** version 2 (aka PCRE) can match elements of an input string according to a given pattern and substitute them according to a replacement (another pattern), so let's use it to reverse a string. We need to provide the library an input string, a pattern and a replacement.

To reverse the input string, the pattern should be of the form `(.)`... (as many as there are characters) which capture one character at a time in what's called a "capture group". The replacement string should contain group references (a number preceded by a dollar sign) in reverse order. For example:
* Input string: `"abcdef"`
* Pattern: `"(.)(.)(.)(.)(.)(.)"`
* Replacement: `"$7$6$5$4$3$2$1"`

The pcre2 library needs to be installed (`sudo apt install libpcre2-dev` on Debian derivatives)
```c
void pcre2_revstr(char *string) {
	int length = strlen(string);

	char *pattern = create_pattern(length);
	char *replacement = create_replacement(length);

	// Compile regex into binary
	pcre2_code *re = pattern_compile(pattern);

	char *output = substitute(re, string, replacement, length);

	// Copy the reversed string into the original one
	strcpy(string, (char*) output);
}
```
The [source code](src/pcre2_revstr.c) is much more involved.

There are limitations with pcre2, for example we can capture at most 65,535 groups, but in our case there's yet another more stringent limitation: the length of the pattern that the library can compile limits the input strings to 7,280 characters.

And as one would expect from using a library for what it's not really intended to, it's slow as hell.

## Unit Testing
The testing program in [util/unittest.c](util/unittest.c) is built by using: `make`.

The resulting executable is called `unittest` and runs every function with varying string length from 0 (empty string) to 1024.

Padding is also added before and after the string, and it is checked that those paddings are left unchanged by the function call. The alignment of the input string is also randomized.

```plain
$ ./unittest
Initializing RNG seed to 1642194155
awk spawned as pid 123456
Testing ptr_revstr()... all tests PASSED (1024 tests)
Testing array_revstr()... all tests PASSED (1024 tests)
Testing array_revstr_simpler()... all tests PASSED (1024 tests)
Testing array_revstr_xor()... all tests PASSED (1024 tests)
Testing recurse_revstr()... all tests PASSED (1024 tests)
Testing malloc_revstr()... all tests PASSED (1024 tests)
Testing random_revstr()... all tests PASSED (1024 tests)
Testing duff_revstr()... all tests PASSED (1024 tests)
Testing bits64_revstr()... all tests PASSED (1024 tests)
Testing linked_list_revstr()... all tests PASSED (1024 tests)
Testing stack_revstr()... all tests PASSED (1024 tests)
Testing bubble_revstr()... all tests PASSED (1024 tests)
Testing partition_revstr()... all tests PASSED (1024 tests)
Testing scanf_revstr()... all tests PASSED (1024 tests)
Testing btree_descent_revstr()... all tests PASSED (1024 tests)
Testing btree_pop_max_revstr()... all tests PASSED (1024 tests)
Testing awk_revstr()... all tests PASSED (1024 tests)
Testing circular_revstr()... all tests PASSED (1024 tests)
Testing ring_revstr()... all tests PASSED (1024 tests)
Testing file_revstr()... all tests PASSED (1024 tests)
Testing pcre2_revstr()... all tests PASSED (1024 tests)
All tests passed!
```

For test reproducibility, the RNG seed is displayed and can be used when given as argument (e.g. `./unittest 1642194155`)

## Benchmarking
The benchmarking program in [util/benchmarking.c](util/benchmarking.c) is built by using: `make`.

The resulting executable is called `benchmark` and runs every function during 100ms against three types of strings:
* an empty string
* a short string of 256 characters
* a long string of 7,280 bytes (the longest we can do with pcre2)

The unit is: number of reverse string function calls per second.

```plain
$ ./benchmark
awk spawned as pid 123456
┌────────────────────────┬──────────────┬──────────────┬─────────────┐
│     Implementation     │ empty string │ short string │ long string │
│                        │              │ (256 chars)  │ (~7k chars) │
├────────────────────────┼──────────────┼──────────────┼─────────────┤
│ ptr_revstr()           │    6,504,180 │    3,595,100 │     263,930 │
│ array_revstr()         │    6,861,410 │    4,670,660 │     495,730 │
│ array_revstr_simpler() │    6,810,200 │    4,043,170 │     339,490 │
│ array_revstr_xor()     │    6,736,400 │    3,644,550 │     272,130 │
│ recurse_revstr()       │    6,834,210 │    4,622,490 │     491,670 │
│ malloc_revstr()        │    6,105,040 │    3,718,250 │     378,390 │
│ random_revstr()        │    5,939,210 │       99,430 │       2,270 │
│ duff_revstr()          │    6,882,680 │    4,750,250 │     491,970 │
│ bits64_revstr()        │    6,855,800 │    6,152,770 │   1,988,480 │
│ linked_list_revstr()   │    6,619,370 │      235,700 │       7,590 │
│ stack_revstr()         │    5,803,220 │    2,875,400 │     238,190 │
│ bubble_revstr()        │    6,803,770 │       58,530 │          80 │
│ opencl_revstr()        │    5,688,790 │        4,780 │       3,100 │
│ partition_revstr()     │    6,120,940 │      437,730 │      28,740 │
│ scanf_revstr()         │    4,191,160 │      178,410 │       2,570 │
│ btree_descent_revstr() │    4,679,000 │       70,860 │       1,930 │
│ btree_pop_max_revstr() │    4,615,190 │       60,860 │       1,660 │
│ awk_revstr()           │    6,733,340 │      203,290 │      29,580 │
│ circular_revstr()      │    6,546,110 │       50,430 │          70 │
│ ring_revstr()          │    6,606,440 │        8,000 │          20 │
│ file_revstr()          │      119,030 │        9,990 │         420 │
│ pcre2_revstr()         │    1,181,380 │        1,610 │          10 │
└────────────────────────┴──────────────┴──────────────┴─────────────┘
```
Higher numbers imply higher performance.

Ideally CPU caches should be flushed between each benchmark result, but that would make the benchmarking program less portable and wouldn't probably change much the results.

## Acknowledgements

Thanks to Jacob for the inspiration and the source material.
