// vim: sw=8:ts=8:noet:ai

#ifndef __REVSTR_H__
#define __REVSTR_H__

typedef void (*revstr_func)(char*);

struct function_desc {
	char *name;
	revstr_func func;
};

void ptr_revstr(char*);
void array_revstr(char*);
void array_revstr_simpler(char*);
void array_revstr_xor(char*);
void recurse_revstr(char*);
void malloc_revstr(char*);
void random_revstr(char*);
void duff_revstr(char*);
void bits64_revstr(char*);
void linked_list_revstr(char*);
void stack_revstr(char*);
void bubble_revstr(char*);
void partition_revstr(char*);
void scanf_revstr(char*);
void btree_pop_max_revstr(char*);
void btree_descent_revstr(char*);
void awk_revstr(char*);
void circular_revstr(char*);
void ring_revstr(char*);
void file_revstr(char*);
void pcre2_revstr(char*);

void awk_init(void);
void awk_fini(void);

#endif //__REVSTR_H__
