FROM ubuntu:focal

RUN DEBIAN_FRONTEND=noninteractive apt-get update -qq
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y -qq \
    gcc make pkg-config gawk
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y -qq \
    pocl-opencl-icd ocl-icd-libopencl1 ocl-icd-opencl-dev opencl-headers llvm clinfo

RUN which gcc make
RUN gcc --version
RUN make --version
