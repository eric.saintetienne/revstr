// vim: sw=8:ts=8:noet:ai

LIB_SRCS = src/btree/btree.c	\
	   src/functions.c	\
	   src/ptr_revstr.c	\
	   src/array_revstr.c	\
	   src/recurse_revstr.c	\
	   src/malloc_revstr.c	\
	   src/random_revstr.c	\
	   src/duff_revstr.c	\
	   src/64bits_revstr.c	\
	   src/linked_list_revstr.c \
	   src/stack_revstr.c	\
	   src/bubble_revstr.c	\
	   src/partition_revstr.c \
	   src/scanf_revstr.c	\
	   src/btree_revstr.c	\
	   src/awk_revstr.c	\
	   src/circular_revstr.c \
	   src/ring_revstr.c	\
	   src/file_revstr.c	\
	   src/pcre2_revstr.c
LIB_OBJS = $(LIB_SRCS:.c=.o)
LIB_TARGET = librevstr.a

TEST_SRCS = util/unittest.c
TEST_OBJS = $(TEST_SRCS:.c=.o)
TEST_TARGET = unittest

BENCH_SRCS = util/benchmark.c	\
	     util/libfort/fort.c
BENCH_OBJS = $(BENCH_SRCS:.c=.o)
BENCH_TARGET = benchmark

FILES_TO_CLEAN = $(LIB_TARGET)   $(LIB_OBJS)	\
		 $(TEST_TARGET)  $(TEST_OBJS)	\
		 $(BENCH_TARGET) $(BENCH_OBJS)

CFLAGS = -O3 -Wall
LDFLAGS = $(shell pkg-config --libs libpcre2-8)

all: library tests benchmark

library: $(LIB_TARGET)

$(LIB_TARGET): $(LIB_OBJS)
	@echo AR $@ $V
	@$(AR) rc -o $@ $^

tests: $(TEST_TARGET)

$(TEST_TARGET): $(TEST_OBJS) $(LIB_TARGET)
	@echo LD $@ $V
	@$(CC) $^ $(LDFLAGS) -o $@

$(BENCH_TARGET): $(BENCH_OBJS) $(LIB_TARGET)
	@echo LD $@ $V
	@$(CC) $^ $(LDFLAGS) -o $@

%.o: %.c revstr.h
	@echo CC $@
	@$(CC) -c -o $@ $(CFLAGS) $<

clean:
	@echo RM $(FILES_TO_CLEAN)
	@$(RM) $(FILES_TO_CLEAN)

.PHONY: all library tests clean
